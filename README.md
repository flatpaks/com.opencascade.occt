# OpenCASCADE flatpak

A flatpak for [OpenCASCADE](https://www.opencascade.com/).

Type `flatpak run com.opencascade.occt` to run the draw.sh OpenCASCADE executable.

This project is also a good starting point for users that want to package an app depending on OpenCASCADE. Feel free to look at the manifest!

Tests are included in the flatpak: for instance, run OpenCASCADE executable and type `testgrid blend` in the console to run the blend suite test.

More information about OpenCASCADE usage can be found in the [online documentation](https://dev.opencascade.org/doc/overview/html/index.html).

## Reporting

Issues concerning the flatpak should be reported on the issue tracker linked to this repository.

Issues concerning OpenCascade should be reported [on the OpenCASCADE bug tracker](https://tracker.dev.opencascade.org/my_view_page.php), or [on the OpenCASCADE forum](https://www.opencascade.com/forums/usage-issues).

See also [the OpenCASCADE developpers ressources](https://dev.opencascade.org/index.php?q=home/resources), and more generally the [development portal](http://dev.opencascade.org).

## License

Both OpenCASCADE and this flatpak are published under [LGPL 2.1 license](./LICENSE).
